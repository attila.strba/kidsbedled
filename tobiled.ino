//#include <ArduinoJson.h>
//#include <ESP8266WiFi.h>
//#include <PubSubClient.h>
//#include <ArduinoOTA.h>
//#include <WiFiUdp.h>
//#include <ESP8266mDNS.h>
#include "FastLED.h"
//API from FASTLED: http://fastled.io/docs/3.1/class_c_fast_l_e_d.html#a6819ff831058a710d4582dfc09817202
// Go to the sketch config IncludeLibrary to install fastled

//Whole Home assistant example: https://github.com/bruhautomation/ESP-MQTT-JSON-Digital-LEDs
//ESP8266 https://arduino-esp8266.readthedocs.io/en/latest/faq/a01-espcomm_sync-failed.html
//PIR: https://github.com/MKme/Wemos-D1-ESP8266-PIR-Alarm

#define NUM_LEDS    20
#define DATA_PIN    5
//#define CLOCK_PIN 5
#define CHIPSET     WS2811
#define COLOR_ORDER BRG
struct CRGB leds[NUM_LEDS];
#define MAX_BRIGHTNESS 55     // Thats full on, watch the power!
#define MIN_BRIGHTNESS 0 // set to a minimum of 25%
#define FADE_SPEED 3
int sensor = 13;  // Digital pin D7

void setup() {
  Serial.begin(115200);
  FastLED.addLeds<CHIPSET, DATA_PIN, COLOR_ORDER>(leds, NUM_LEDS);
  FastLED.setBrightness(MAX_BRIGHTNESS);


  pinMode(sensor, INPUT);   // declare sensor as input
  Serial.println("Setup Ready");

}


void setColor(int inR, int inG, int inB) {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i].red   = inR;
    leds[i].green = inG;
    leds[i].blue  = inB;
  }
  FastLED.show();
};



/*
  Serial.println("Setting LEDs:");
  Serial.print("r: ");
  Serial.print(inR);
  Serial.print(", g: ");
  Serial.print(inG);
  Serial.print(", b: ");
  Serial.println(inB);*/


void showleds()
{
  setColor(0, 255, 255);
  for (int br = MIN_BRIGHTNESS; br <= MAX_BRIGHTNESS; br++)
  {
    FastLED.setBrightness(br);
    FastLED.show();
    FastLED.delay(FADE_SPEED);

  };

  FastLED.delay(5000);

  for (int br = MAX_BRIGHTNESS; br > MIN_BRIGHTNESS; br--)
  {
    FastLED.setBrightness(br);
    FastLED.show();
    FastLED.delay(FADE_SPEED);

  };
  setColor(0, 0, 0);
}

void loop() {
  long state = digitalRead(sensor);
  if (state == HIGH) {
    showleds();
  }

 // setColor(0, 0, 0);


  // put your main code here, to run repeatedly:

}
