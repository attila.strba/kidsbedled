This project is work in progress. I am trying to implement a color leds
for my kids bed.

For this I have used these links:
* //API from FASTLED: http://fastled.io/docs/3.1/class_c_fast_l_e_d.html#a6819ff831058a710d4582dfc09817202
* //Whole Home assistant example: https://github.com/bruhautomation/ESP-MQTT-JSON-Digital-LEDs
* //ESP8266 https://arduino-esp8266.readthedocs.io/en/latest/faq/a01-espcomm_sync-failed.html
* //PIR: https://github.com/MKme/Wemos-D1-ESP8266-PIR-Alarm
